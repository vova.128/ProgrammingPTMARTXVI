package ua.org.oa.vova_128.task4_2.part1;

import java.util.Arrays;

public class Demo {
    public static void main(String[] args) {
        MyDeque<Number> deque1 = new MyDequeImpl<Number>();
        deque1.addFirst(433);
        deque1.addLast(8.88);
        deque1.addLast(0);
        deque1.addFirst(-57);

        MyDeque<Number> deque2 = new MyDequeImpl<Number>();
        deque2.addFirst(-57);
        deque2.addLast(0);
        deque2.addFirst(8.88);

        System.out.println(deque1.toString());
        System.out.println("list contains 433 --> " + deque1.contains(433));
        System.out.println("list contains " + deque2.toString() + " --> " + deque1.containsAll(deque2));
        System.out.println("size: " + deque1.size());
        System.out.println("an array of elements: "
                + Arrays.asList(deque1.toArray()));
        System.out.println("~~~~~~~~~~~~~~~~~~~~");
        System.out.println("getting element from the beginning without removing it: "
                + deque1.getFirst());
        System.out.println("getting element from the end without removing it: "
                + deque1.getLast());
        System.out.println(deque1.toString());
        System.out.println("~~~~~~~~~~~~~~~~~~~~");
        System.out.println("getting element from the beginning with removing it: "
                + deque1.removeFirst());
        System.out.println("getting element from the end with removing it: "
                + deque1.removeLast());
        System.out.println(deque1.toString());

    }
}
