package ua.org.oa.vova_128.task4_2.part3;

public interface ListIterable<E> {
    ListIterator<E> listIterator();
}
