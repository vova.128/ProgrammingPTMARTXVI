package ua.org.oa.vova_128.task4_2.part2;

import java.util.Iterator;

public class Demo {

    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<Number>();
        deque.addFirst(433);
        deque.addLast(8.88);
        deque.addLast(0);
        deque.addFirst(-57);

        System.out.println(deque.toString());

        System.out.println("~~~~~~~~~~~~~~~~~~~~");

        for (Number element : deque) {
            System.out.println(element);
        }

        System.out.println("~~~~~~~~~~~~~~~~~~~~");

        Iterator<Number> it = deque.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
