package ua.org.oa.vova_128.task4_2.part3;

public class Demo {
    private int x = 1;

    public static void main(String[] args) {

        MyDeque<Number> deque = new MyDequeImpl<Number>();
        deque.addFirst(433);
        deque.addLast(8.88);
        deque.addLast(0);
        deque.addFirst(-57);

        System.out.println(deque.toString());

        System.out.println("~~~~~~~~~~~~~~~~~~~~");

        ListIterator<Number> listIt = deque.listIterator();

        while (listIt.hasNext()) {
            System.out.println(listIt.next());
        }

        System.out.println("~~~~~~~~~~~~~~~~~~~~");

        while (listIt.hasPrevious()) {
            System.out.println(listIt.previous());
        }

    }

}
