package ua.org.oa.vova_128.task4_1;

import java.util.Arrays;

public class Maximum {

    public static<T extends Comparable<T>> void getMaxElement(T[] array) {
        Arrays.sort(array, (T o1, T o2) -> {
            return (o1.compareTo(o2));
        });
//        System.out.println("sorted array: " + Arrays.asList(array));
        System.out.println("max element = " + array[array.length-1] + '\n');
    }

    public static void main(String[] args) {
        Integer[] masInteger = {2, 4, 5, 10, -8};
        String[] masString = {"word1", "word5", "word4", "word3", "word2"};
        Auto[] masAuto = {new Auto(6, 2.3), new Auto(4, 2.4), new Auto(5, 3.1)};
        Computer[] masComputer = {new Computer("Windows 7", 2.4), new Computer("Windows 10", 3.1),
                new Computer("Windows 08", 3.1), new Computer("Linux Mint", 2.5)};
        Maximum.getMaxElement(masInteger);
        Maximum.getMaxElement(masString);
        Maximum.getMaxElement(masComputer);
//        Maximum.getMaxElement(masAuto);
    }
}
