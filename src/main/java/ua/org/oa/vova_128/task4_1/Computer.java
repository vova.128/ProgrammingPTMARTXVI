package ua.org.oa.vova_128.task4_1;

public class Computer implements Comparable<Computer>{
    private String os;
    private double cpuFrequency;

    public Computer(String os, double cpuFrequency) {
        this.os = os;
        this.cpuFrequency = cpuFrequency;
    }

    @Override
    public int compareTo(Computer computer) {
        int result = Double.valueOf(this.cpuFrequency).compareTo(computer.cpuFrequency);
        if(result != 0) {
            return result;
        } else {
            return String.valueOf(this.os).compareTo(computer.os);
        }
    }

    @Override
    public String toString() {
        return "Computer{" +
                "os = '" + os + '\'' +
                ", cpuFrequency = " + cpuFrequency +
                '}';
    }
}
