package ua.org.oa.vova_128.task4_1;

public class Auto {
    private int transmission;
    private double engine;

    public Auto(int transmission, double engine) {
        this.transmission = transmission;
        this.engine = engine;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "transmission = " + transmission +
                ", engine = " + engine +
                '}';
    }
}
