package org.ua.panasenko.Practice1;

/**
 * Created by Vladimir on 04.04.2016.
 */
public class Student {
    private String firstName;
    private String lastName;
    private Group group;
    private Exam[] exam;

    public Student(String firstName, String lastName, Group group, Exam[] exam){
        this.firstName = firstName;
        this.lastName = lastName;
        this.group = group;
        this.exam = exam;
    }

    public int getHighestMark(String subject){
        int highestMark = 0;
        for (Exam ex : exam) {
            if(ex.getSubjectName().equals(subject) && highestMark < ex.getMark()){
                highestMark = ex.getMark();
            }
        }
        return highestMark;
    }

    public void deleteMark(String subjectName){
        for (Exam ex : exam) {
            if (ex.getSubjectName().equals(subjectName)) {
                ex.setMark(0);
            }
        }
    }

    public void setMark(int mark, String subjectName){
        for (Exam ex : exam) {
            if (ex.getSubjectName().equals(subjectName)) {
                ex.setMark(mark);
            }
        }
    }

    public int getCountOfExam(int mark){
        int count = 0;
        for (Exam exam1 : exam) {
            if (exam1.getMark() == mark){
                count++;
            }
        }
        return count;
    }

    public double getAverageMark(){
        int count = 0;
        int generalMark = 0;
        for (Exam exam1 : exam) {
            count++;
            generalMark += exam1.getMark();
        }
        return generalMark / (count * 1.0);
    }

    public boolean isPassed(String subjectName) throws Exception{
        boolean isPassed = false;
        for (Exam exam1 : exam) {
            if (subjectName.equals(exam1.getSubjectName())){
                isPassed = true;
                return isPassed;
            }
        }
        throw new Exception();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Student{" +
                "firstName = " + firstName
                + ", lastName = " + lastName
                + ", " + group + ",\nExams: ");
        for (Exam exam1 : exam) {
            sb.append(exam1.toString());
        }
        return sb.append('}').toString();
    }

    public static void main(String[] args) throws Exception{
        Student student = new Student("Vladimir", "Ivanov",
                new Group(3, "Computer science"),
                new Exam[]{new Exam("Math", 4, "21.12.2013"),
                        new Exam("Physics", 4, "13.12.2013"),
                        new Exam("Physics", 5, "05.10.2014"),
                        new Exam("Programming", 4, "02.11.2015")});
        System.out.println(student.isPassed("Programming"));
    }

}
