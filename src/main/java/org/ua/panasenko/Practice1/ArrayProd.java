package org.ua.panasenko.Practice1;

/**
 * Created by Vladimir on 04.04.2016.
 */
public class ArrayProd {
    public int Prod(int[] Array){
        int result = 1;
        for (int i = 0; i < Array.length; i++)
            result *= Array[i];
        return result;
    }
}
