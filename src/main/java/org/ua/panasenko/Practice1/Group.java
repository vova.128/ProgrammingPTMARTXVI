package org.ua.panasenko.Practice1;

/**
 * Created by Vladimir on 04.04.2016.
 */
public class Group {
    private int course;
    private String faculty;

    public Group(int course, String faculty){
        this.course = course;
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        return "course = " + course
                + ", faculty = " + faculty;
    }
}
