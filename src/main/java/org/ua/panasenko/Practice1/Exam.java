package org.ua.panasenko.Practice1;

/**
 * Created by Vladimir on 04.04.2016.
 */
public class Exam {
    private String subjectName;
    private int mark;
    private String date;

    public Exam(String subjectName, int mark, String date) {
        this.subjectName = subjectName;
        this.mark = mark;
        this.date = date;
    }

    public void setMark(int mark){
        this.mark = mark;
    }

    public int getMark(){
        return mark;
    }

    public String getSubjectName() {
        return subjectName;
    }

    @Override
    public String toString() {
        return "nameOfExam = " + subjectName
                    + ", mark = " + mark
                    + ", date = " + date + "; ";
    }

}
