package org.ua.panasenko.Practice3;

public class GenericStorage<K, V> {
    private Object[] objectArray = null;
    private int size = 0;
    private int ind = 0;

    public GenericStorage(int size) {
        objectArray = new Object[size];
    }

    public boolean add(K obj){
        if (ind <= objectArray.length - 1){
            objectArray[ind] = obj;
            size++;
            ind++;
            return true;
        }
        else{
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public K[] getAll(){
        Object[] array = new Object[size];
        int j = 0;
        for (Object o : objectArray) {
            if (o != null){
                array[j] = o;
                j++;
            }
        }
        return (K[])array;
    }

    public int getSize(){
        return size;
    }
}
