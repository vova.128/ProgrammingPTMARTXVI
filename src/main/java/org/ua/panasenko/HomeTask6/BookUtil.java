package org.ua.panasenko.HomeTask6;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BookUtil {

    public static String BOOK = "src\\main\\java\\org\\ua\\panasenko\\HomeTask6\\book.txt";

    public List<Book> fillCollectionFromFile(){
        List<Book> list = new ArrayList<>();
        File file = new File(BookUtil.BOOK);
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(new FileInputStream(file), "windows-1251"))) {
            String l;
            while ((l = br.readLine()) != null){
                if (!l.isEmpty()) {
                    String[] str = l.split(";");
                    list.add(new Book(str[1], str[0], Integer.parseInt(str[2])));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void addBookToFile(Book book){
        File file = new File(BookUtil.BOOK);
        try (BufferedWriter bw = new BufferedWriter
                (new OutputStreamWriter(new FileOutputStream(file, true),"windows-1251"));
             BufferedReader br = new BufferedReader(
                     new InputStreamReader(new FileInputStream(file), "windows-1251"))){
            String l = br.readLine();
            if (l == null){
                bw.write(book.getAuthor());
                bw.write(";");
                bw.write(book.getTitle());
                bw.write(";");
                bw.write(Integer.toString(book.getYear()));
            } else {
                bw.write('\n');
                bw.write(book.getAuthor());
                bw.write(";");
                bw.write(book.getTitle());
                bw.write(";");
                bw.write(Integer.toString(book.getYear()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String toString(List list){
        if (list.size() == 0){
            return "File is empty";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i == list.size() - 1){
                sb.append(list.get(i));
            } else {
                sb.append(list.get(i)).append('\n');
            }
        }
        return sb.toString();
    }

    public static void main(String[] args){
        BookUtil bu = new BookUtil();
        Book book = new Book("Java: ������ �����������", "������� �����", 2012);
        bu.addBookToFile(book);
        System.out.print(bu.toString(bu.fillCollectionFromFile()));
    }
}

/*
����������� �.�.;������� ������;2014
�������� �.;451' �� ����������;2015
�������� �.�.;������ � ���������;2014
������� ������;���� ���, ��� ����� ���;2009
��������;������� � ������;2011
*/
