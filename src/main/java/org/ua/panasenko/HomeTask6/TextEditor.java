package org.ua.panasenko.HomeTask6;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class TextEditor {

    public static void createFile(String pathDirectory, String fileName){
        File file = new File(pathDirectory.concat("\\").concat(fileName));
        try {
            if (!file.createNewFile()) {
                System.out.println("File \"" + fileName + "\" already exists");
            } else {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createFolder(String pathDirectory, String folderName){
        File folder = new File(pathDirectory.concat("\\").concat(folderName));
        if (!folder.exists()){
            folder.mkdir();
        } else {
            System.out.println("Folder \"" + folderName + "\" already exists");
        }
    }

    public static void deleteFile(String pathDirectory, String fileName){
        File file = new File(pathDirectory.concat("\\").concat(fileName));
        if(file.exists()){
            file.delete();
        } else {
            System.out.print("File not found!");
        }
    }

    public static void renameFile(String pathDirectory, String oldFileName, String newFileName){
        File file = new File(pathDirectory.concat("\\").concat(oldFileName));
        if(file.exists()){
            file.renameTo(new File(pathDirectory.concat("\\").concat(newFileName)));
        } else {
            System.out.print("File not found!");
        }
    }

    public static void getListOfFiles(String pathDirectory){
        File directory = new File(pathDirectory);
        String[] files = directory.list();
        if (files.length == 0){
            System.out.print("Directory is empty");
        } else {
            for (String file : files) {
                System.out.println(file);
            }
        }
    }

    public static void commandExecution(){
        System.out.println("Available commands: createFile(cf), deleteFile(df), renameFile(rf), createFolder(cfo), getFiles(gf)");
        Scanner sc = new Scanner(System.in);

        String command = null;
        String pathDirectory = null;
        String fileName = null;

        System.out.print("Type the command: ");
        command = sc.nextLine();

        switch (command) {
            case "cf":
                System.out.print("Type the path directory: ");
                pathDirectory = sc.nextLine();
                System.out.print("Type the filename: ");
                fileName = sc.nextLine();
                TextEditor.createFile(pathDirectory, fileName);
                break;
            case "df":
                System.out.print("Type the path directory: ");
                pathDirectory = sc.nextLine();
                System.out.print("Type the filename: ");
                fileName = sc.nextLine();
                TextEditor.deleteFile(pathDirectory, fileName);
                break;
            case "rf":
                System.out.print("Type the path directory: ");
                pathDirectory = sc.nextLine();
                System.out.print("Type the old filename: ");
                String oldFileName = sc.nextLine();
                System.out.print("Type the new filename: ");
                String newFileName = sc.nextLine();
                TextEditor.renameFile(pathDirectory, oldFileName, newFileName);
                break;
            case "cfo":
                System.out.print("Type the path directory: ");
                pathDirectory = sc.nextLine();
                System.out.print("Type the folder name: ");
                String folderName = sc.nextLine();
                TextEditor.createFolder(pathDirectory, folderName);
                break;
            case "gf":
                System.out.print("Type the path directory: ");
                pathDirectory = sc.nextLine();
                TextEditor.getListOfFiles(pathDirectory);
                break;
            default:
                System.err.println("Incorrect command");
                break;
        }

        System.out.print("\nContinue [Y]es / [N]o: ");
        String isContinue = sc.nextLine();
        if (isContinue.equals("Y")){
            System.out.println();
            TextEditor.commandExecution();
        }
        sc.close();
    }

    public static void main(String[] args) {
        TextEditor.commandExecution();
    }
}
