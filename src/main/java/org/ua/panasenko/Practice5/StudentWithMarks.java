package org.ua.panasenko.Practice5;

import java.util.List;

public class StudentWithMarks {
    private String firstName;
    private String lastName;
    private List<Integer> marks;

    public StudentWithMarks(){

    }

    public StudentWithMarks(String firstName, String lastName, List<Integer> marks) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.marks = marks;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Integer> getMarks() {
        return marks;
    }

    public void setMarks(List<Integer> marks) {
        this.marks = marks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StudentWithMarks student = (StudentWithMarks) o;

        return firstName.equals(student.firstName) && lastName.equals(student.lastName);
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (marks != null ? marks.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", marks=" + marks;
    }

    public double average(){
        double allMarks = 0;
        for (Integer integer : marks) {
            allMarks += integer;
        }
        return allMarks / marks.size();
    }
}
