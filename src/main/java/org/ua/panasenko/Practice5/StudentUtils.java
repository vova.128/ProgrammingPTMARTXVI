package org.ua.panasenko.Practice5;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StudentUtils {

    static String STUDENTS = "src\\main\\java\\org\\ua\\panasenko\\Practice5\\students.txt";

    public List<StudentWithMarks> createListOfStudents() {
        List<StudentWithMarks> list = new ArrayList<>();
        List<Integer> marks = new ArrayList<>();
        File file = new File(StudentUtils.STUDENTS);
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()){
                String[] str = scanner.nextLine().split("\\s");
                StudentWithMarks student = new StudentWithMarks();
                student.setFirstName(str[0]);
                student.setLastName(str[1]);
                marks.add(Integer.parseInt(str[3]));
                student.setMarks(marks);
                marks = new ArrayList<>();
                this.addStudent(student, list);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void addStudent(StudentWithMarks student, List<StudentWithMarks> list){
        int index = 0;
        StudentWithMarks addedStudent = null;
        if(list.size() == 0 || !list.contains(student)){
            list.add(student);
        } else {
            for(int i = 0; i < list.size(); i++){
                if(list.get(i).equals(student)){
                    index = i;
                    break;
                }
            }
            addedStudent = list.get(index);
            addedStudent.getMarks().add(student.getMarks().get(0));
            student.setMarks(addedStudent.getMarks());
            list.set(index, student);
        }
    }

    public List<StudentWithMarks> checkAverageMark(List<StudentWithMarks> list) {
        List<StudentWithMarks> studentList = new ArrayList<>();
        for (StudentWithMarks student : list) {
            if (student.average() > 90){
                studentList.add(student);
            }
        }
        return studentList;
    }

    public String toString(List<StudentWithMarks> list){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i == list.size() - 1){
                sb.append(list.get(i));
            } else {
                sb.append(list.get(i)).append('\n');
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) throws IOException {
        StudentUtils su = new StudentUtils();
        System.out.println(su.toString(su.checkAverageMark(su.createListOfStudents())));
    }

}
