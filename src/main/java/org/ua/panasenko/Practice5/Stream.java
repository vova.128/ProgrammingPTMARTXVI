package org.ua.panasenko.Practice5;

import java.io.*;
import java.util.*;

public class Stream {

    static String NUMBERS = "src\\main\\java\\org\\ua\\panasenko\\Practice5";
    static String CHANGE_WORDS = "src\\main\\java\\org\\ua\\panasenko\\Practice5\\changeWords.txt";
    static String COPY_FROM = "src\\main\\java\\org\\ua\\panasenko\\Practice5\\copyFrom.txt";
    static String COPY_TO = "src\\main\\java\\org\\ua\\panasenko\\Practice5\\copyTo.txt";

    public File createFile(String fileName){
        Random random = new Random();
        File file = new File(Stream.NUMBERS.concat("\\").concat(fileName));
        try {
            if (!file.createNewFile()) {
                System.out.println("File \"" + fileName + "\" already exists");
                return file;
            } else {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            for (int i = 0; i < 10; i++){
                bw.write(Integer.toString(random.nextInt(200) - 100));
                bw.append(' ');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public List sort(File file) {
        ArrayList<Integer> list = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()){
                list.add(scanner.nextInt());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.sort(list);
        return list;
    }

    public String changeWords(String pathName) {
        StringBuilder sb = new StringBuilder();
        File file = new File(pathName);
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(new FileInputStream(file), "windows-1251"))) {
            String l;
            while( (l = br.readLine()) != null){
                sb.append(l);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] str = sb.toString().split("\\."); // ��������� �� �����������
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < str.length; i++){
            String[] temporary = str[i].trim().split("\\s"); // ����������� ��������� �� �����
            if (temporary[0].contains(",")){
                temporary[0] = temporary[0].replace(",", "");
            }
            String temp = temporary[0]; // ������ ������� ������ � ��������� �����
            temporary[0] = temporary[temporary.length - 1];
            temporary[temporary.length - 1] = temp;
            for (int j = 0; j < temporary.length; j++) { // ���������� ���������
                if (j == temporary.length - 1){
                    res.append(temporary[j]).append(".\n");
                } else {
                    res.append(temporary[j]).append(' ');
                }
            }
        }
        return res.toString().trim();
    }

    public void copy() {
        File copyFrom = new File(Stream.COPY_FROM);
        File copyTo = new File(Stream.COPY_TO);

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(new FileInputStream(copyFrom), "windows-1251"));
             BufferedWriter bw = new BufferedWriter(
                     new OutputStreamWriter(new FileOutputStream(copyTo, false)));
             InputStreamReader isr = new InputStreamReader(
                     new FileInputStream(copyFrom), "windows-1251");
             FileWriter fw = new FileWriter(copyTo)) {

            int c;
            long timeBefore = System.nanoTime();
            while( (c = br.read()) != -1){
                bw.write(c);
            }
            long timeResultBuffered = System.nanoTime() - timeBefore;
            System.out.printf(Locale.GERMAN, "Buffered stream: %,d nanoseconds%n", timeResultBuffered);

            timeBefore = System.nanoTime();
            while( (c = isr.read()) != -1){
                fw.write(c);
            }
            long timeResultUnbuffered = System.nanoTime() - timeBefore;
            System.out.printf(Locale.GERMAN, "Unbuffered stream: %,d nanoseconds%n", timeResultUnbuffered);

            System.out.printf(Locale.GERMAN, "Difference = %,d nanoseconds%n", (timeResultUnbuffered - timeResultBuffered));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        Stream stream = new Stream();
        File file = stream.createFile("numbers.txt");
        System.out.println(stream.sort(file));
        System.out.println("~~~~~~~~~~");
        stream.copy();
        System.out.println("~~~~~~~~~~");
        System.out.print(stream.changeWords(Stream.CHANGE_WORDS));
    }
}
