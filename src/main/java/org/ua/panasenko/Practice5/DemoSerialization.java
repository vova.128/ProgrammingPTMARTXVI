package org.ua.panasenko.Practice5;

import java.io.*;

public class DemoSerialization {

    public static void main(String[] args) {
        Group group = new Group();
        group.addStudent(new Student("Bogdan", "Suprun"));
        group.addStudent(new Student("Vladimir", "Panasenko"));
        group.addStudent(new Student("Vadim", "Fomin"));

        try (ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream("src\\main\\java\\org\\ua\\panasenko\\Practice5\\serialization.txt"))) {
            oos.writeObject(group);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream("src\\main\\java\\org\\ua\\panasenko\\Practice5\\serialization.txt"))) {
            Group obj = (Group)ois.readObject();
            System.out.print(obj);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
