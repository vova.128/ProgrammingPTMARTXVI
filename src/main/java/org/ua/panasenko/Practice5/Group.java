package org.ua.panasenko.Practice5;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Group implements Serializable{

    private static final long serialVersionUID = -6750268860931195940L;
    private List<Student> list = new ArrayList<>();

    public void addStudent(Student student){
        list.add(student);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Group:\n");
        for (Student student : list) {
            sb.append(student).append('\n');
        }
        return sb.toString();
    }

}
