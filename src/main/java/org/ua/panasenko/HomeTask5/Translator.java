package org.ua.panasenko.HomeTask5;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Translator {

    public static String DIRECTORY = "src\\main\\java\\org\\ua\\panasenko\\HomeTask5";

    public static Map<String, String> readDictionary(String dictionaryName){
        File file = new File(Translator.DIRECTORY.concat("\\").concat(dictionaryName));
        Map<String, String> dictionary = new HashMap<>();
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(new FileInputStream(file), "windows-1251"))) {
            String l;
            while ((l = br.readLine()) != null){
                String[] str = l.split("=");
                dictionary.put(str[0], str[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dictionary;
    }

    public static String translate(String fileName, Map<String, String> dictionary){
        File file = new File(Translator.DIRECTORY.concat("\\").concat(fileName));
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(new FileInputStream(file), "windows-1251"))) {
            String l;
            while ((l = br.readLine()) != null){
                String[] str = l.split("[\\s\\.]+");
                for (String s : str) {
                    if (dictionary.containsKey(s)){
                        sb.append(dictionary.get(s)).append(' ');
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString().trim().concat(".");
    }

    public static void execution() throws Exception {
        Scanner sc = new Scanner(System.in);

        System.out.print("Type the filename to translate: ");
        String fileName = sc.nextLine();
        File file = new File(Translator.DIRECTORY.concat("\\").concat(fileName));
        if(!file.exists()){
            throw new Exception("There is no such file");
        }

        if (fileName.contains("ru")){
            System.out.print("Available translation directions: ru-en, ru-ua\n");
        } else if (fileName.contains("en")){
            System.out.print("Available translation directions: en-ru, en-ua\n");
        } else if (fileName.contains("ua")){
            System.out.print("Available translation directions: ua-ru, ua-en\n");
        }

        System.out.print("Type the translation direction: ");
        String translationDirection = sc.nextLine();
        String dictionaryName = translationDirection.concat(".txt");
        file = new File(Translator.DIRECTORY.concat("\\").concat(dictionaryName));
        if(!file.exists()){
            throw new Exception("There is no such dictionary");
        }

        System.out.println("Translation: " +
                Translator.translate(fileName, Translator.readDictionary(dictionaryName)));

        System.out.print("\nContinue [Y]es / [N]o: ");
        String isContinue = sc.nextLine();
        if (isContinue.equals("Y")){
            System.out.println();
            Translator.execution();
        }
        sc.close();
    }

    public static void main(String[] args) throws Exception {
        Translator.execution();
    }
}
