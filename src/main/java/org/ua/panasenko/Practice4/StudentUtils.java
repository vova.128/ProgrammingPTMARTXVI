package org.ua.panasenko.Practice4;

import java.util.*;

public class StudentUtils {

    static Map<String, Student> createMapFromList(List<Student> students){
        Map<String, Student> student = new HashMap<>();
        for (Student st : students) {
            student.put(st.getFirstName() + " " + st.getLastName(), st);
        }
        for (Map.Entry<String, Student> entry : student.entrySet()) {
            System.out.println(entry);
        }
        return student;
    }

    static void printStudentsInCourse(List<Student> students, int course) {
        Iterator<Student> iterator = students.iterator();
        while (iterator.hasNext()){
            Student st = iterator.next();
            if (st.getCourse() == course) {
                System.out.println(st.getFirstName() + " " + st.getLastName());
            }
        }
    }

    static List<Student> sortStudent(List<Student> students){
/*
        Collections.sort(students, (student1, student2) -> student1.getFirstName().compareTo(student2.getFirstName()));
*/
        Collections.sort(students, new Comparator<Student>() {
            @Override
            public int compare(Student student1, Student student2) {
                return student1.getFirstName().compareTo(student2.getFirstName());
            }
        });
        for (Student student : students) {
            System.out.println(student);
        }
        return students;
    }

    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student("Vladimir", "Panasenko", 3));
        students.add(new Student("Alexandr", "Ivanov", 2));
        students.add(new Student("Alexey", "Pavlov", 3));

        StudentUtils.createMapFromList(students);

        System.out.println("~~~~~~~~~~~~~~~~~~~~");

        StudentUtils.printStudentsInCourse(students, 3);

        System.out.println("~~~~~~~~~~~~~~~~~~~~");

        StudentUtils.sortStudent(students);
    }
}
