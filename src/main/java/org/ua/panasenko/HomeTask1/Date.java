package org.ua.panasenko.HomeTask1;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Date {
    private int day;
    private int month;
    private int year;

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    //���� ������
    public Day getDayOfWeek(){
        GregorianCalendar gc = new GregorianCalendar(getYear(), getMonth() - 1, getDay());
        return Day.valueOf(gc.get(Calendar.DAY_OF_WEEK) - 2);
    }

    //���������� ����� ��� � ����
    public int getDayOfYear(){
        GregorianCalendar gc = new GregorianCalendar(getYear(), getMonth() - 1, getDay());
        return gc.get(Calendar.DAY_OF_YEAR);
    }

    //���������� ���� ����� ����� ������
    public int getDaysBetween(Date date) {
        GregorianCalendar first = new GregorianCalendar();
        first.set(getYear(), getMonth() - 1, getDay());
        Calendar second = GregorianCalendar.getInstance();
        second.set(date.getYear(), date.getMonth() - 1, date.getDay());
        long daysBetween = Math.abs(second.getTimeInMillis() - first.getTimeInMillis());
        daysBetween = daysBetween / 1000 / 3600 / 24;
        return (int)daysBetween;
    }

    @Override
    public String toString() {
        return day + "." + month + "." + year;
    }

    class Year {
        private int yearNumber;
        private boolean isLeap;

        public Year(int yearNumber){
            this.yearNumber = yearNumber;
            GregorianCalendar gc = new GregorianCalendar();
            isLeap = gc.isLeapYear(yearNumber);
        }

        public int getYearNumber() {
            return yearNumber;
        }

        public boolean getIsLeap() {
            return isLeap;
        }
    }

    class Month {

        public int getDaysInMonth(int month, boolean isLeap) {
            switch (month) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    return 31;
                case 2:
                    if (isLeap){
                        return 29;
                    }
                    else {
                        return 28;
                    }
                case 4:
                case 6:
                case 9:
                case 11:
                    return 30;
                default:
                    return 30;
            }
        }
    }

    public enum Day {
        MONDAY(0), TUESDAY(1), WEDNESDAY(2), THURSDAY(3), FRIDAY(4), SATURDAY(5), SUNDAY(6);
        public int index;

        private Day(int index) {
            this.index = index;
        }

        public static Day valueOf(int index) {
            switch (index) {
                case 0:
                    return MONDAY;
                case 1:
                    return TUESDAY;
                case 2:
                    return WEDNESDAY;
                case 3:
                    return THURSDAY;
                case 4:
                    return FRIDAY;
                case 5:
                    return SATURDAY;
                case 6:
                    return SUNDAY;
            }
            return null;
        }

    }

    public static void main(String[] args) {
        Date date1 = new Date(15, 4, 2016);
        System.out.println(date1);
        System.out.println("���� ������: " + date1.getDayOfWeek());
        Date date2 = new Date(22, 11, 2018);
        System.out.println("���������� ���� ����� " + date2 + " � " + date1
                + " = " + date1.getDaysBetween(date2));
        System.out.println("���������� ����� ��� � ���� " + date1
                + " = " + date1.getDayOfYear());

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        Month month = date1.new Month();
        System.out.println("���-�� ���� � ������: " + month.getDaysInMonth(2, true));

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        Year year = date1.new Year(2018);
        System.out.println(year.getYearNumber() + " ���������� ���: " + year.getIsLeap());
    }
}