package org.ua.panasenko.HomeTask1;

public class Auto {
    protected String name;
    protected String color;
    protected int year;

    public Auto(String name, int year, String color) {
        this.name = name;
        this.year = year;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Auto{"
                + "name=" + name
                + ", color=" + color
                + ", year=" + year + '}';
    }

    public static void main(String[] args) {
        Auto auto1 = new Auto("BMW x6", 2013, "black"){
            @Override
            public String toString(){
                return "����{"
                        + "��������=" + this.name
                        + ", ����=" + this.color
                        + ", ���=" + this.year + '}';
            }

            @Override
            public boolean equals(Object obj) {
                if (this == obj)
                    return true;
                if (obj == null)
                    return false;
                if (getClass() != obj.getClass())
                    return false;
                Auto other = (Auto) obj;
                if (name != other.name)
                    return false;
                return true;
            }
        };
        Auto auto2 = new Auto("BMW x6", 2013, "black"){
            @Override
            public String toString(){
                return "����������{"
                        + "������=" + this.name
                        + ", �����=" + this.color
                        + ", ��� �������=" + this.year + '}';
            }

            @Override
            public boolean equals(Object obj) {
                if (this == obj)
                    return true;
                if (obj == null)
                    return false;
                if (getClass() != obj.getClass())
                    return false;
                Auto other = (Auto) obj;
                if (year != other.year)
                    return false;
                return true;
            }
        };
        System.out.println(auto1);
        System.out.println(auto2);
        System.out.println(auto1.equals(auto2));
        System.out.println(auto2.equals(auto1));
    }
}
