package org.ua.panasenko.Practice2;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

    public static final String PATH1 = "C:\\Users\\Vladimir\\workspace\\ProgrammingPTMARXVI" +
            "\\src\\main\\java\\org\\ua\\panasenko\\Practice2\\baby2008.html";
    public static final String PATH2 = "C:\\Users\\Vladimir\\workspace\\ProgrammingPTMARXVI" +
            "\\src\\main\\java\\org\\ua\\panasenko\\Practice2\\source.html";

    public static char[] readFile(String path){
        File file = new File(path);
        char[] buffer = null;
        try{
            buffer = new char[(int)file.length()];
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(new FileInputStream(path), "windows-1251"));
            br.read(buffer);
            br.close();
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        return buffer;
    }

    public static ArrayList<Rating> parseNameRating(char[] buffer){
        String str = new String(buffer);
        ArrayList<Rating> res = new ArrayList<>();
        Pattern p = Pattern.compile("<td>(\\d+)</td><td>([a-zA-Z]+)</td><td>([a-zA-Z]+)</td>");
        Matcher m = p.matcher(str);
        while (m.find()) {
            Rating rating = new Rating(Integer.parseInt(m.group(1)), m.group(2), m.group(3));
            res.add(rating);
        }
        return res;
    }

    public static ArrayList<Notebook> parseNotebook(char[] buffer){
        String str = new String(buffer);
        ArrayList<Notebook> res = new ArrayList<>();

        String regex1 = "\\)\">(.+)</a></h6>"; // название ноутбука
        String regex2 = "<p class=\"description\">(\\d{2},\\d)";
        String regex3 = "Процессор - (.+ГГц)";
        String regex4 = "(\\d{2})\\sмес."; // гарантия
        String regex5 = "<span class=\"price cost\">(\\d{4})"; // стоимость

        Pattern p1 = Pattern.compile(regex1);
        Matcher m1 = p1.matcher(str);

        Pattern p2 = Pattern.compile(regex2);
        Matcher m2 = p2.matcher(str);

        Pattern p3 = Pattern.compile(regex3);
        Matcher m3 = p3.matcher(str);

        Pattern p4 = Pattern.compile(regex4);
        Matcher m4 = p4.matcher(str);

        Pattern p5 = Pattern.compile(regex5);
        Matcher m5 = p5.matcher(str);

        while (m1.find() && m2.find() && m3.find()
                && m4.find() && m5.find()) {
            Notebook notebook = new Notebook(m1.group(1), Integer.parseInt(m5.group(1)),
                    m2.group(1), m3.group(1), Integer.parseInt(m4.group(1)));
            res.add(notebook);
        }
        return res;
    }

    public static String toString(ArrayList list) {
        StringBuilder sb = new StringBuilder();
        for (Object o : list) {
            sb.append(o);
            sb.append('\n');
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.printf(Parser.toString(parseNameRating(Parser.readFile(Parser.PATH1))));
        System.out.println();
        System.out.printf(Parser.toString(parseNotebook(Parser.readFile(Parser.PATH2))));
    }
}