package org.ua.panasenko.HomeTask2;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    public StringUtils() {
    }

    public static String upend(String str){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++){
            sb.append(str.charAt(str.length() - i - 1));
        }
        return sb.toString();
    }

    public static boolean isPalindrome(String str){
        str = str.toLowerCase();
        String[] array = str.split("[\\s,.!-]+");
        StringBuilder res = new StringBuilder();
        for (String s : array) {
            res.append(s);
        }
        return res.toString().equals(upend(res.toString()));
    }

    public static String completeO(String str){
        if (str.length() > 10){
            str = str.substring(0, 6);
        }
        else{
            StringBuilder sb = new StringBuilder(str);
            for (int i = 0; i < 12 - str.length(); i++){
                sb.append('o');
            }
            str = sb.toString();
        }
        return str;
    }

    public static String changeWord(String str){
        String[] array = str.split("\\s+");
        String temp = array[0];
        array[0] = array[array.length - 1];
        array[array.length - 1] = temp;
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < array.length; i++){
            if (i == array.length - 1){
                res.append(array[i]);
            }
            else{
                res.append(array[i]);
                res.append(" ");
            }
        }
        return res.toString();
    }

    public static String changeWordInEachSentence(String str){
        String[] array = str.split("\\.");
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i].trim();
            array[i] = changeWord(array[i]);
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < array.length; i++){
            if (i == array.length - 1){
                sb.append(array[i]);
                sb.append(".");
            }
            else{
                sb.append(array[i]);
                sb.append(". ");
            }
        }
        return sb.toString();
    }

    public static boolean checkABC(String str){
        Pattern p = Pattern.compile("[abc]+");
        Matcher m = p.matcher(str);
        return m.matches();
    }

    public static boolean isDate(String str){
        Pattern p = Pattern.compile("(0[1-9]|1[012]).(0[1-9]|1[0-9]|2[0-9]|3[01]).([0-9]{4})");
        Matcher m = p.matcher(str);
        return m.matches();
    }

    public static boolean isEmail(String str){
        Pattern p = Pattern.compile("^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+"
                + "(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$");
        Matcher m = p.matcher(str);
        return m.matches();
    }

    public static ArrayList<String> findPhoneNumber(String str){
        Pattern p = Pattern.compile("^\\+\\d\\(\\d{3}\\)\\d{3}-\\d{2}-\\d{2}$");
        Matcher m;
        String[] array = str.split("\\s+");
        ArrayList<String> res = new ArrayList<>();
        for (String s : array) {
            m = p.matcher(s);
            if (m.matches()){
                res.add(s);
            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println("��������� ������: " + StringUtils.upend("Hello world!"));
        System.out.println("���������: " + StringUtils.isPalindrome("� ���� ����� �� ���� �����!"));
        System.out.println("���������� ������ ��������� 'o': " + StringUtils.completeO("football"));
        System.out.println("����� ������� ������� � ���������� ����� � �������: "
                + StringUtils.changeWord("hello my wonderful world"));
        System.out.println("����� ������� ������� � ���������� ����� � ������ �����������: "
                + StringUtils.changeWordInEachSentence("����� �� ����� ����������� ����� " +
                "��������������� ������� � ������� ��� ����� �� ����� �����. �������� ������� ��������� " +
                "�� 2 ������ � ����� ���� ������� ��� �� 1 �����. ����� ����, ��� ��� " +
                "������������ ������� �������� ����� ����."));
        System.out.println("�������� �� ������ ������ ������� 'a', 'b', 'c': "
                + StringUtils.checkABC("abbcdfg"));
        System.out.println("����: " + StringUtils.isDate("12.10.2016"));
        System.out.println("�������� �����: " + StringUtils.isEmail("wovan096@mail.ru"));
        System.out.println("��������: " + StringUtils.findPhoneNumber("��� ��������: " +
                "+3(123)456-78-90 +3(456)789-01-02 � ��� +3(789)011-02-03"));
    }
}
