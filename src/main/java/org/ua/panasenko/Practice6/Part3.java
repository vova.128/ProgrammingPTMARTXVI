package org.ua.panasenko.Practice6;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Part3 extends Thread {

    public static final String DIRECTORY_PATH = "src\\main\\java\\org\\ua\\panasenko\\Practice6";
    public static final String LOG_FILE = "src\\main\\java\\org\\ua\\panasenko\\Practice6\\log.txt";

    public void searchFiles(String directoryPath) {
        File directory = new File(directoryPath);
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(LOG_FILE), true))) {
                    if (file.getName().contains(".txt")) {
                        bw.write(file.getAbsolutePath() + '\n');
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (file.isDirectory()) {
                new Thread(() -> searchFiles(directoryPath.concat("\\").concat(file.getName()))).start();
            }
        }
    }

    @Override
    public void run() {
        searchFiles(DIRECTORY_PATH);
    }

    public static void main(String[] args) {
        Thread thread = new Part3();
        thread.start();
    }
}
