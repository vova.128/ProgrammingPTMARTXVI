package org.ua.panasenko.Practice6;

public class Main {

    public static void main(String[] args) {
        Runnable runnable = new RunnableClass("Runnable");
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new ThreadClass("Thread");
        thread1.start();
        thread2.start();
    }
}

class ThreadClass extends Thread {

    public ThreadClass(String name) {
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(this.getName());
            try {
                sleep(500);
            } catch (InterruptedException e) {
                System.err.println("Error");
            }
        }
    }

}

class RunnableClass implements Runnable {

    private Thread thread;

    public RunnableClass(String name) {
        thread = new Thread(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(thread.getName());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.err.println("Error");
            }
        }
    }
}
