package org.ua.panasenko.Practice6;

import java.util.*;

public class MyShedule extends Thread {
    Map<String, Integer> map = new LinkedHashMap<>();

    public MyShedule(String[] msg, int[] time) {
        for (int i = 0; i < msg.length; i++){
            map.put(msg[i], time[i]);
        }
    }

    @Override
    public void run() {
        try {
            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                Thread.sleep(entry.getValue());
                System.out.println(entry.getKey());
            }
        } catch (InterruptedException e) {
            System.err.println("Error");
        }
    }

    public static void main(String[] args) {
        int[] time = {2000, 500, 3000, 100};
        String[] msg = {"world", "java", "knure", "sport"};
        Thread thread = new MyShedule(msg, time);
        thread.start();
    }

}