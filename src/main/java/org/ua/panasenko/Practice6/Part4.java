package org.ua.panasenko.Practice6;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class Part4 extends Thread {

    public final static String COPY_FROM = "src\\main\\java\\org\\ua\\panasenko\\Practice6\\copyFrom";
    public final static String COPY_TO = "src\\main\\java\\org\\ua\\panasenko\\Practice6\\copyTo";

    public void copyFiles(String copyFrom, String copyTo) {
        File directoryCopyFrom = new File(copyFrom);
        try {
            File[] mas = directoryCopyFrom.listFiles();
            for (File file : mas) {
                if (file.isFile()) {
                    String fileName = copyTo.concat("\\").concat(file.getName());
                    File newFile = new File(fileName);
                    Files.copy(directoryCopyFrom.toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);}
//                } else if (file.isDirectory()) {
//                    new Thread(() -> copyFiles(file.getAbsolutePath(), copyTo.concat("\\").concat(file.getName()))).start();
//                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        copyFiles(COPY_FROM, COPY_TO);
    }

    public static void main(String[] args) {
        Thread thread = new Part4();
        thread.start();
    }
}