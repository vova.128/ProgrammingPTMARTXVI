package org.ua.panasenko.HomeTask3;

import java.util.Arrays;

public class Demo {

    public static String checkOperation(boolean bool){
        if (bool){
            return "�������� ��������� �������";
        }
        else{
            return "�������� �� �������";
        }
    }

    public static void main(String[] args) {
        GenericStorage<String> genericStorage = new GenericStorage<String>(4);
        System.out.println(checkOperation(genericStorage.add("Real Madrid")));
        System.out.println(checkOperation(genericStorage.add("Manchester �ity")));
        System.out.println(checkOperation(genericStorage.add("Atletico de Madrid")));
        System.out.println(checkOperation(genericStorage.add("Bayern Munchen")));
        System.out.println(Arrays.asList(genericStorage.getAll()));
        System.out.println("������� ������ �������: " + genericStorage.getSize());

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println(genericStorage.get(2));

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println(checkOperation(genericStorage.update(3, "Barcelona")));
        System.out.println(Arrays.asList(genericStorage.getAll()));

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println(checkOperation(genericStorage.delete(2)));
        System.out.println("������� ������ �������: " + genericStorage.getSize());
        System.out.println(Arrays.asList(genericStorage.getAll()));

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println(checkOperation(genericStorage.delete("Barcelona")));
        System.out.println("������� ������ �������: " + genericStorage.getSize());
        System.out.println(Arrays.asList(genericStorage.getAll()));
    }

}
