package org.ua.panasenko.HomeTask3;

public class GenericStorage<T> {
    private Object[] objectArray = null;
    private int size = 0;
    private int ind = 0;
    public static final int ARRAY_CAPACITY = 10;

    public GenericStorage() {
        objectArray = new Object[ARRAY_CAPACITY];
    }

    public GenericStorage(int size) {
        objectArray = new Object[size];
    }

    public boolean add(T obj){
        if (ind <= objectArray.length - 1){
            objectArray[ind] = obj;
            size++;
            ind++;
            return true;
        }
        else{
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public T get(int index){
        if (index < 0 || index >= objectArray.length){
            return null;
        }
        else{
            return (T)objectArray[index];
        }
    }

    @SuppressWarnings("unchecked")
    public T[] getAll(){
        Object[] array = new Object[size];
        int j = 0;
        for (Object o : objectArray) {
            if (o != null){
                array[j] = o;
                j++;
            }
        }
        return (T[])array;
    }

    public boolean update(int index, T obj){
        if (index < 0 || index >= objectArray.length){
            return false;
        }
        if (obj == null || objectArray[index] == null){
            return false;
        }
        objectArray[index] = obj;
        return true;
    }

    public boolean delete(int index){
        if (index < 0 || index >= objectArray.length){
            return false;
        }
        if (objectArray[index] == null){
            return false;
        }
        else{
            objectArray[index] = null;
            size--;
            return true;
        }
    }

    public boolean delete(T obj){
        if (obj == null){
            return false;
        }
        for (int i = 0; i < objectArray.length; i++) {
            if (obj.equals(objectArray[i])){
                objectArray[i] = null;
                size--;
            }
        }
        return true;
    }

    public int getSize(){
        return size;
    }

}
