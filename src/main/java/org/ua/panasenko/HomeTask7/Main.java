package org.ua.panasenko.HomeTask7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        interruptThreadClass();
        interruptRunnableClass();
    }

    //завершение потока путем ввода любого символа с клавиатуры
    public static void interruptThreadClass() {
        Thread thread = new ThreadClass();
        thread.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        thread.interrupt();
    }

    //завершение потока путем ввода любого символа с клавиатуры
    public static void interruptRunnableClass() {
        Runnable runnable = new RunnableClass();
        Thread thread = new Thread(runnable);
        thread.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        thread.interrupt();
    }
}

class ThreadClass extends Thread {
    DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    Date date = null;

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            date = new Date();
            System.out.println(dateFormat.format(date));
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                System.err.println("Thread created by ThreadClass is interrupted\n");
                Thread.currentThread().interrupt();
            }
        }
    }
}

class RunnableClass implements Runnable {
    DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    Date date = null;

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            date = new Date();
            System.out.println(dateFormat.format(date));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.err.println("Thread created by RunnableClass is interrupted\n");
                Thread.currentThread().interrupt();
            }
        }
    }
}