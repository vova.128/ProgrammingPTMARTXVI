package org.ua.panasenko.HomeTask7;

import java.io.IOException;

public class Part3 {

    private int a;
    private int b;

    public Part3() {
        this.a = 0;
        this.b = 0;
    }

    public void incrementA() {
        a++;
    }

    public void incrementB() {
        b++;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public static void main(String[] args) throws IOException,
            InterruptedException {
        RunnableNotSynchr.main(args);
        Thread.sleep(500);
        RunnableSynchr.main(args);
    }
}

class RunnableNotSynchr implements Runnable {

    private Part3 p;

    public RunnableNotSynchr(Part3 p) {
        this.p = p;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.print("a = " + p.getA() + ", b = " + p.getB() + ": ");
            if (p.getA() > p.getB()) {
                System.out.println("a > b");
            }
            if (p.getB() > p.getA()) {
                System.out.println("b > a");
            }
            if (p.getA() == p.getB()) {
                System.out.println("a = b");
            }
            p.incrementA();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                System.out.println("Error");
            }
            p.incrementB();
        }
    }

    public static void main(String[] args) throws IOException,
            InterruptedException {
        Part3 p = new Part3();
        Runnable runnable = new RunnableNotSynchr(p);
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
    }

}

class RunnableSynchr implements Runnable {

    private Part3 p;

    public RunnableSynchr(Part3 p) {
        this.p = p;
    }

    @Override
    public synchronized void run() {
        for (int i = 0; i < 5; i++) {
            System.out.print("a = " + p.getA() + ", b = " + p.getB() + ": ");
            if (p.getA() > p.getB()) {
                System.out.println("a > b");
            }
            if (p.getB() > p.getA()) {
                System.out.println("b > a");
            }
            if (p.getA() == p.getB()) {
                System.out.println("a = b");
            }
            p.incrementA();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                System.out.println("Error");
            }
            p.incrementB();
        }
    }

    public static void main(String[] args) throws IOException {
        System.out.println("\n----------Synchronized block----------\n");
        Part3 p = new Part3();
        Runnable runnable = new RunnableSynchr(p);
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
    }
}
