package org.ua.panasenko.Practice1;

import org.junit.Test;

import static org.junit.Assert.*;

public class ArrayProdTest {

    @Test
    public void testProd1() throws Exception {
        int actual = 120;
        int mas[] = {1, 2, 3, 4, 5};
        int expected = new ArrayProd().Prod(mas);
        double delta = 0.01;
        assertEquals(expected, actual, delta);
    }
}