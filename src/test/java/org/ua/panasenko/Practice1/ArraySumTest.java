package org.ua.panasenko.Practice1;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ArraySumTest{

    ArraySum ar;

    @Test
    public void testSum1() throws Exception {
        int actual = 15;
        int mas[] = {1, 2, 3, 4, 5};
        int expected = ArraySum.Sum(mas);
        double delta = 0.01;
        assertEquals(expected, actual, delta);
    }

    @Before
    public void setUp() throws Exception {
        ar = new ArraySum(new int[]{1, 2, 3});
    }

    @Test
    public void testSum2() throws Exception {
        int actual = 6;
        int expected = ar.Sum();
        double delta = 0.01;
        assertEquals(expected, actual, delta);
    }

    @Test(expected = java.lang.NullPointerException.class)
    public void testSum3() {
        ArraySum.Sum(null);
    }
}