package org.ua.panasenko.Practice1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StudentTest {

    Student student = new Student("Vladimir", "Ivanov",
            new Group(3, "Computer science"),
            new Exam[]{new Exam("Math", 4, "21.12.2013"),
                    new Exam("Physics", 4, "13.12.2013"),
                    new Exam("Physics", 5, "05.10.2014"),
                    new Exam("Programming", 4, "02.11.2015")});

    @Test
    public void testGetHighestMark() throws Exception {
        assertEquals(5, student.getHighestMark("Physics"), 0.01);
    }

    @Test
    public void testDeleteMark() throws Exception {
        student.deleteMark("Math");
    }

    @Test
    public void testSetMark() throws Exception {
        student.setMark(5, "Math");
    }

    @Test
    public void testGetCountOfExam() throws Exception {
        assertEquals(3, student.getCountOfExam(4), 0.01);
    }

    @Test
    public void testGetAverageMark() throws Exception {
        student.getAverageMark();
    }

    @Test(expected = java.lang.Exception.class)
    public void testIsPassed() throws Exception {
        student.isPassed("Chemistry");
    }
}
