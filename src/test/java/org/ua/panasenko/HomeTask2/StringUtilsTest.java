package org.ua.panasenko.HomeTask2;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class StringUtilsTest {

    @Test
    public void testUpend() {
        assertEquals(StringUtils.upend("Hello world!"), "!dlrow olleH");
    }

    @Test
    public void testIsPalindrome() {
        assertTrue(StringUtils.isPalindrome("� ���� ����� �� ���� �����"));
    }

    @Test
    public void testCompleteO() {
        assertEquals(StringUtils.completeO("football"), "footballoooo");
    }

    @Test
    public void testChangeWord() {
        assertEquals(StringUtils.changeWord("hello my wonderful world"),
                "world my wonderful hello");
    }

    @Test
    public void testChangeWordInEachSentence() {
        assertEquals(StringUtils.changeWordInEachSentence("������ ����. " +
                "���� ����� ����. ���� ����."),
                "���� ������. ���� ����� ����. ���� ����.");
    }

    @Test
    public void testCheckABC() {
        assertFalse(StringUtils.checkABC("abbcfbd"));
    }

    @Test
    public void testIsDate() {
        assertTrue(StringUtils.isDate("12.10.2016"));
    }

    @Test
    public void testIsEmail() {
        assertTrue(StringUtils.isEmail("wovan096@mail.ru"));
    }

    @Test
    public void testFindPhoneNumber() {
        StringUtils.findPhoneNumber("��� ��������: " +
                "+3(123)456-78-90 +3(456)789-01-02 � ��� +3(789)011-02-03");
    }
}